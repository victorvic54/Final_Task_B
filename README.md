# TASK B
## Section C Part 1

**Prerequisite**: npm and nodejs are installed

Steps to run the API locally:
1. git clone this repo
2. open terminal window (lets say t1) with terminal path at the root path of the github repository folder

3. On t1, do the following
-	cd backend
-	npm install
-	npm install nodemon
-	nodemon

4. Backend service will be up at `localhost:5000`

Constraint and edge cases tested:
- username and title should be length of at least 3 after trimmed
- invalid input such as missing field in the JSON body etc will be handled (server don’t crashed)

Functionality tested:

- ✔ should add a user's exercise record 
- ✔ cannot create exercises with username and title too short
- ✔ cannot create exercises with username too short after trimmed
- ✔ should get all exercises record 
- ✔ should update a user's exercise record 
- ✔ cannot update username and title that is too short 
- ✔ should delete a user's exercise record 
- ✔ should check final number of exercises after deletion


### [CREATE] exercises
```
URL:  http://localhost:5000/exercises/add
Method: POST 
Expected Result: “Exercise added!”
Example Body: 

{
   "username": "victor",
   "title": "CS3219 Lecture 1",
   "description": "attend lecture on 04/09"
}
```

### [GET] all exercises
```
URL:  http://localhost:5000/exercises
Method: GET 
Example Body: -
Example Result: 

{
      "_id": "61334adde73bb7cf6292820e",
      "username": "victor",
      "title": "CS3219 Lecture 1",
      "description": "attend lecture on 04/09",
      "createdAt": "2021-09-04T10:30:53.876Z",
      "updatedAt": "2021-09-04T10:30:53.876Z",
      "__v": 0
}
```

### [UPDATE] exercises

```
URL: http://localhost:5000/exercises/update/{id}

Method: POST 
Expected Result: “Exercise updated!”
Example Body:

{
   "username": "victor",
   "title": "CS3219 Lecture 1",
   "description": "attend lecture on 12/09"
}
```

Note: <br>
`{id}` in the URL above can be received from `[GET] exercises` result under `"_id"` field.

In this case: http://localhost:5000/exercises/update/61334adde73bb7cf6292820e


### [DELETE] exercises
```
URL: http://localhost:5000/exercises/{id}

Method: DELETE 
Expected Result: “Exercise deleted!”
Example Body: -
```

Note: <br>
`{id}` in the URL above can be received from `[GET] exercises` result under `"_id"` field.

In this case, http://localhost:5000/exercises/61334adde73bb7cf6292820e

<br>
<hr>

## Section C Part 2
Access deployed API: 

Endpoints:
- POST - https://ydjcroltdd.execute-api.us-east-1.amazonaws.com/dev/exercises/add
- GET - https://ydjcroltdd.execute-api.us-east-1.amazonaws.com/dev/exercises
- PUT - https://ydjcroltdd.execute-api.us-east-1.amazonaws.com/dev/exercises/{id}
- DELETE - https://ydjcroltdd.execute-api.us-east-1.amazonaws.com/dev/exercises/{id}

<br>
<hr>

## Section C Part 3
To run test locally:
On your current terminal t1, run: `npm run test`

Continuous Integration and Continuous Deployment are made through `Gitlab CI/CD pipeline`

Additional images will be shown in the report (below)

<br>
<hr>

## Section C Part 4

Open another terminal with terminal path at the root path of the github repository folder, run (also make sure the backend is running):
-	npm install
-	npm start
