import React, { Component } from 'react';
import axios from 'axios';

export default class EditExercise extends Component {
  constructor(props) {
    super(props);

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      username: '',
      title: '',
      description: ''
    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/exercises/'+this.props.match.params.id)
      .then(response => {
        this.setState({
          username: response.data.username,
          title: response.data.title,
          description: response.data.description,
        })   
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value.trim()
    })
  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value.trim()
    })
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const exercise = {
      username: this.state.username,
      title: this.state.title,
      description: this.state.description,
    }

    console.log(exercise);

    axios.post('http://localhost:5000/exercises/update/' + this.props.match.params.id, exercise)
      .then(res => console.log(res.data));

    setTimeout(function(){
      window.location = '/';
    }, 1000);
  }

  render() {
    return (
    <div>
      <h3>Edit Exercise Log</h3>
      <br/>
      <form onSubmit={this.onSubmit}>
        <div className="form-group"> 
          <label>Username: </label>
          <input type="text"
              required
              minLength="3"
              className="form-control"
              value={this.state.username.trim()}
              onChange={this.onChangeUsername}
              />
        </div>
        <br/>
        <div className="form-group"> 
          <label>Title: </label>
          <input type="text"
              required
              minLength="3"
              className="form-control"
              value={this.state.title.trim()}
              onChange={this.onChangeTitle}
              />
        </div>
        <br/>
        <div className="form-group"> 
          <label>Description: </label>
          <input type="text"
              required
              className="form-control"
              value={this.state.description}
              onChange={this.onChangeDescription}
              />
        </div>
        <br/>

        <div className="form-group">
          <input type="submit" value="Edit Exercise Log" className="btn btn-primary" />
        </div>
      </form>
    </div>
    )
  }
}