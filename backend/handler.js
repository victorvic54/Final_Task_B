require('dotenv').config({ path: './.env' });

const isEmpty = require('lodash.isempty');
const validator = require('validator');
const connectToDatabase = require('./db');
const Exercise = require('./models/exercise.model');


const createErrorResponse = (statusCode, message) => ({
  statusCode: statusCode || 501,
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    error: message || 'An Error occurred.',
  }),
});

const returnError = (error) => {
  console.log(error);
  if (error.name) {
    const message = `Invalid ${error.path}: ${error.value}`;
    callback(null, createErrorResponse(400, `Error:: ${message}`));
  } else {
    callback(
      null,
      createErrorResponse(error.statusCode || 500, `Error:: ${error.name}`)
    );
  }
};

module.exports.create = async (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  if (isEmpty(event.body)) {
    return callback(null, createErrorResponse(400, 'Missing details'));
  }
  const { username, title, description } = JSON.parse(
    event.body
  );

  const newExercise = new Exercise({
    username,
    title,
    description,
  });

  if (newExercise.validateSync()) {
    return callback(null, createErrorResponse(400, 'Incorrect exercise details'));
  }

  try {
    await connectToDatabase();
    console.log(newExercise);
    const exercise = await Exercise.create(newExercise);
    callback(null, {
      statusCode: 200,
      body: JSON.stringify(exercise),
    });
  } catch (error) {
    returnError(error);
  }
};

module.exports.getAll = async (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  try {
    await connectToDatabase();
    const exercise = await Exercise.find();
    if (!exercise) {
      callback(null, createErrorResponse(404, 'No Exercise Found.'));
    }

    callback(null, {
      statusCode: 200,
      body: JSON.stringify(exercise),
    });
  } catch (error) {
    returnError(error);
  }
};

module.exports.update = async (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const data = JSON.parse(event.body);

  if (!validator.isAlphanumeric(event.pathParameters.id)) {
    callback(null, createErrorResponse(400, 'Incorrect Id.'));
    return;
  }

  if (isEmpty(data)) {
    return callback(null, createErrorResponse(400, 'Missing details'));
  }

  const { username, title, description } = data;

  try {
    await connectToDatabase();

    const exercise = await Exercise.findById(event.pathParameters.id);

    if (exercise) {
      exercise.username = username || exercise.username;
      exercise.title = title || exercise.title;
      exercise.description = description || exercise.description;
    }

    const newExercise = await exercise.save();

    callback(null, {
      statusCode: 204,
      body: JSON.stringify(newExercise),
    });
  } catch (error) {
    returnError(error);
  }
};

module.exports.delete = async (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const id = event.pathParameters.id;

  if (!validator.isAlphanumeric(id)) {
    callback(null, createErrorResponse(400, 'Incorrect Id.'));
    return;
  }

  try {
    await connectToDatabase();
    const exercise = await Exercise.findByIdAndRemove(id);
    callback(null, {
      statusCode: 200,
      body: JSON.stringify({
        message: `Removed exercise with id: ${exercise._id}`,
        exercise,
      }),
    });
  } catch (error) {
    returnError(error);
  }
};