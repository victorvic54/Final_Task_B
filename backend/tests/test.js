// Import the dependencies for testing
var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../server');
var expect = chai.expect;

// Configure chai
chai.use(chaiHttp);

function filterByID(item) {
    if (item.username === "victor" && item.title === "CS3219 lecture 1") {
        return true
    }

    return false;
}

describe("Exercises App", () => {
    let init = 0;
    let id = "";

    // [CREATE] Test to add a user's exercise record
    step("should add a user's exercise record", (done) => {
        chai.request(app)
            .post('/exercises/add')
            .send({
                "username": "victor",
                "title": "CS3219 lecture 1",
                "description": "attend lecture on 04/09"
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });

    // [CREATE] should failed
    step("cannot create exercises with username and title too short", (done) => {
        chai.request(app)
            .post('/exercises/add')
            .send({
                "username": "xx",
                "title": "yy",
                "description": "attend lecture on 04/09"
            })
            .end((err, res) => {
                expect(res).to.have.status(400);
                done();
            });
    });

    // [CREATE] should failed
    step("cannot create exercises with username too short after trimmed", (done) => {
        chai.request(app)
            .post('/exercises/add')
            .send({
                "username": "   xx     ",
                "title": "long title",
                "description": "attend lecture on 04/09"
            })
            .end((err, res) => {
                expect(res).to.have.status(400);
                done();
            });
    });

    // [GET] Test to get all exercises record
    step("should get all exercises record", (done) => {
        chai.request(app)
            .get('/exercises')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an.instanceof(Array);
                id = res.body.filter(filterByID)[0]["_id"];
                init = res.body.length;
                done();
            });
    });

    // [UPDATE] Test to update a user's exercise record
    step("should update a user's exercise record", (done) => {
        chai.request(app)
            .post(`/exercises/update/${id}`)
            .send({
                "username": "victor",
                "title": "CS3219 lecture 2",
                "description": "attend lecture on 23/09"
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });

    // [UPDATE] should failed
    step("cannot update username and title that is too short", (done) => {
        chai.request(app)
            .post(`/exercises/update/${id}`)
            .send({
                "username": "aa",
                "title": "bb",
                "description": "attend lecture on 23/09"
            })
            .end((err, res) => {
                expect(res).to.have.status(400);
                done();
            });
    });

    // [DELETE] Test to delete a user's exercise record
    step("should delete a user's exercise record", (done) => {
        chai.request(app)
            .delete(`/exercises/${id}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });

    // [GET] Check final number of exercises after deletion
    step("should check final number of exercises", (done) => {
        chai.request(app)
            .get('/exercises')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.have.length(init - 1);
                done();
            });
    });
});